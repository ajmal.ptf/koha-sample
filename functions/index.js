const functions = require('firebase-functions')
const { app }= require('./server')
// Create and Deploy Your First Cloud Functions
// https://firebase.google.com/docs/functions/write-firebase-functions

const runtimeOpts = {
  timeoutSeconds: 300,
  memory: '1GB'
}
exports.app = functions.runWith(runtimeOpts).https.onRequest(app)
