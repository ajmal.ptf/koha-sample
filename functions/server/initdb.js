import consola from 'consola'
import mongooseLoader from './loaders/mongoose'
require('dotenv').config()
async function init() {
  try {
    await mongooseLoader()
    consola.success('✌️ DB loaded and connected!')
    // int seed
  } catch (e) {
    consola.error(e)
  }
  process.exit(0)
}
init()
