import mongoose, { Schema } from 'mongoose'

const Book = new Schema(
  {
    name: {
      type: String,
      required: 'is required! '
    }
  },
  {
    strict: true,
    timestamps: true,
    toJSON: { virtuals: true },
    toObject: { virtuals: true }
  }
)

export default mongoose.model('Book', Book)
