import BookService from '../services/BookService'
import Controller from './Controller'

class BookController extends Controller {
  constructor() {
    super(new BookService())
    this.getBook = this.getBook.bind(this)
  }
  getBook(req, res, next) {
    const { id } = req.params
    this.exec(this.service.getBook(id), res, next, 201)
  }
}

export default new BookController()
