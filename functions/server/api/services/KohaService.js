import config from '../../config'
import axios from 'axios'
import qs from 'qs'
import { NotFoundError } from '../../lib/Error'
class KohaService {
  constructor() {
    this.token = null
    this.tokenExpire = new Date()
    this.getToken = this.getToken.bind(this)
    this.getBook = this.getBook.bind(this)
  }
  async getToken() {
    console.log('getToken koha')
    if (!this.token || this.tokenExpire < Date.now()) {
      const data = qs.stringify({
        grant_type: 'client_credentials',
        client_id: config.kohaClientId,
        client_secret: config.kohaClientSecret
      })
      const req = {
        method: 'post',
        url: `${config.kohaBaseUrl}/oauth/token`,
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded',
          Accept: 'application/json'
        },
        data: data
      }

      const response = await axios(req)
      this.token = response.data.access_token
      this.tokenExpire = new Date(Date.now() + response.data.expires_in * 1000)
      console.log('getToken koha', this.token, this.tokenExpire)
    }
    return this.token
  }
  async getBook(id) {
    console.log('getBook koha', id)
    await this.getToken()
    const req = {
      method: 'get',
      url: `${config.kohaBaseUrl}/biblios/${id}`,
      headers: {
        Accept: 'application/json',
        Authorization: `Bearer ${this.token}`
      }
    }
    try {
      const response = await axios(req)
      return response.data
    } catch (err) {
      if(err.response.status === 404) {
        throw new NotFoundError('Book not found')
      }
    }

  }
}

export default new KohaService()
