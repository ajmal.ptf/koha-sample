import Book from '../../modals/Book'
import Service from './Service'
import KohaService from './KohaService'
class BookService extends Service {
  constructor() {
    super(Book)
    this.getBook = this.getBook.bind(this)
  }
  async getBook(id) {
    console.log('getBook', id)
    const book = await KohaService.getBook(id)
    return book
  }
}

export default BookService
