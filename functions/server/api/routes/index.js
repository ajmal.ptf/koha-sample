import { Router } from 'express'
import { Auth } from '../middlewares/AuthMiddilware'
import secure from './secure'
export default () => {
  const app = Router()
  app.use('/', Auth, secure())
  return app
}
