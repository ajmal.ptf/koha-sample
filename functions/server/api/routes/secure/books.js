import { Router } from 'express'
import BookController from '../../controlers/BookController'
export default () => {
  const route = Router()
  route.get('/:id', BookController.getBook)
  return route
}
